import java.util.Scanner;
/** 
 * @author Cindy Park
 * 
 * Computer configuration problem:
 * Display the sum of 5 whole numbers entered.
 *
 * Pseudocode:
 * 
 * 1.Display the purpose of the program
 * 2.Ask the user to type in 5 integers.
 * 3.Display the sum of the 5 numbers inputed
 * 
 * credit Savitch, Walter.Java An Introduction to Problem Solving and Programming. Upper Saddle River:Prentice Hall, 2012. Print
 * CSC 130 Programming Assignment 2
 * Last
 */
public class Sum {
	public static void main(String[] args)
	{
		System.out.println("Hello out there!");
		System.out.println("I will add five numbers for you.");
		System.out.println("Enter five integers on a line.");
		//directions are 
		
		int num1, num2, num3, num4, num5;
		//gives a whole number as the Sum 
		
		Scanner keyboard = new Scanner(System.in);
		num1 = keyboard.nextInt();
		num2 = keyboard.nextInt();
		num3 = keyboard.nextInt();
		num4 = keyboard.nextInt();
		num5 = keyboard.nextInt();
		// reads the data 
		
		System.out.println("The sum of those five numbers is");
		System.out.println(num1 + num2 + num3 + num4 + num5);
		//calculates the sum
	}
}
